from tkinter import Tk, END, BOTH, NW, W, HORIZONTAL, StringVar
from tkinter.ttk import Frame, Button, Label, Entry, Treeview, Progressbar
from tkinter.filedialog import askdirectory, askopenfile, askopenfilename

def set_filename_from_dialog( var ):
    result = askopenfilename(initialfile=var.get())
    if result != "":
        var.set(result)

def set_dirname_from_dialog( var ):
    result = askdirectory(initialdir=var.get())
    if result != "":
        var.set(result+"/")


class WrangeGUI(Frame):
    def __init__(self,root,callback):
        super().__init__()
        self.root = root
        self.callback = callback
        self.init_gui()

    def make_row( self, row, label, picker=None):
        var = StringVar()
        Label(self, text=label).grid(row=row, column=0, sticky=W, padx=10, pady=2 )
        Entry(self,textvariable=var).grid(row=row,column=1, padx=10,pady=2, sticky='EW' )
        if picker:
            Button(self,text='...',command=lambda: picker(var)).grid(row=row,column=2, padx=0, pady=2, sticky='W' )
        return var

    def init_gui(self):
        self.master.title("Wrangle GUI")
        self.pack(fill=BOTH, expand=1)

        # arguments
        self.grid_columnconfigure(1, weight=1)
        self.args_cmd      = self.make_row( 0, "Command" )
        self.args_input    = self.make_row( 1, "Input CSV file", picker=set_filename_from_dialog )
        self.args_name     = self.make_row( 2, "Name column #" )
        self.args_name.set(0)
        self.args_data     = self.make_row( 3, "Data column #" )
        self.args_data.set(1)
        self.args_pre      = self.make_row( 4, "Prefix to output filename", picker=set_dirname_from_dialog )
        self.args_post     = self.make_row( 5, "Postfix to output filename" )
        self.args_post.set(".out")
        self.args_start_at = self.make_row( 6, "Start processing at row #" )
        self.args_start_at.set(1)

        # Preview table
        #Label(self, text='Preview').grid(row=7, column=0, padx=10, pady=2, sticky='NW' )
        #tree = Treeview(self)
        #tree.grid(row=7,column=1, padx=10, pady=2, sticky='EW')
        #tree['columns'] = ('foo', 'bar', 'baz')
        #tree.column('foo', width=100, anchor='center')
        #tree.heading('foo', text='Foo')
        #tree.column('bar', width=100, anchor='center')
        #tree.heading('bar', text='Bar')
        #tree.column('baz', width=100, anchor='center')
        #tree.heading('baz', text='Baz')
        ## Same thing, but inserted as first child:
        #tree.insert('', 'end', 'preview', text='The File Name')
        #tree.insert('preview', 'end', text='Line 2')
        #print('ah')

        Label(self, text='Progress').grid(row=9, column=0, padx=10, pady=2, sticky='NW' )
        self.progressbar = Progressbar(self,orient=HORIZONTAL,mode='determinate')
        self.progressbar.grid(row=9,column=1, padx=10,pady=2, sticky='EW' )
        self.progressbar['value'] = 50

        Button(self,text='Run',command=self.run).grid(row=8, column=1, sticky='EW')

    def pb_tick_callback(self,i):
        self.progressbar['value'] = i
        self.root.update_idletasks()
    
    def pb_max_callback(self,i):
        self.progressbar['maximum'] = i
        self.root.update_idletasks()

    def run(self):
        self.callback(
            self.args_input.get(),
            self.args_cmd.get(),
            self.args_pre.get(),
            int(self.args_name.get()),
            self.args_post.get(),
            int(self.args_data.get()),
            int(self.args_start_at.get()),
            lambda i: self.pb_tick_callback(i),
            lambda i: self.pb_max_callback(i)
            )

def start_gui(callback):
    root = Tk()
    root.geometry("700x250")
    app = WrangeGUI(root,callback)
    root.mainloop()