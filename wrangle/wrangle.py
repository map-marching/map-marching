import csv
import subprocess
import argparse
import progressbar
from gui import start_gui

# handle commandline arguments
argparser = argparse.ArgumentParser(description='Runs an external command for every row in a CSV file and pipes each result to a file.')
argparser.add_argument('--cmd', help='Command to run.')
argparser.add_argument('--input', help='Filename of input CSV.')
argparser.add_argument('--name', '-n', metavar='COL', type=int, help='Get filename from which column?', default=0)
argparser.add_argument('--data', '-d', metavar='COL', type=int, help='Get data from which column?', default=2)
argparser.add_argument('--pre', help='Prefix for output filenames.', default='')
argparser.add_argument('--post', help='Postfix for output filenames.', default='')
argparser.add_argument('--start-at', '-s', metavar='ROW', type=int, help='First row to start working at; earlier rows are skipped.', default=1)
argparser.add_argument('--timeout', '-t', metavar='SECONDS', type=float, help='Kill the command after this many seconds.', default=None)
argparser.add_argument('--gui', action="store_true", help='Launch graphical user interface.')
args = argparser.parse_args()

def process_row(row,cmd,pre,name,post,data,cmd_timeout=None):
    command = [cmd]
    filename = pre + row[name] + post
    print('Processing, writing to',filename)
    try:
        with open(filename,'w') as outfile:
            print(filename)
            print(command)
            subprocess.run( command, shell=True, input=row[data], stdout=outfile, universal_newlines=True, timeout=cmd_timeout, check=True )
            print("Success.")
            print()

    except subprocess.CalledProcessError as error:
        print('Command gave nonzero return code')
        with open(filename+'.error'+str(error.returncode),'w') as errorfile:
            print(row[data],file=errorfile)
    
    except subprocess.TimeoutExpired:
        print('Command timed out on row ',row[name])
        with open(filename+'.timeout','w') as errorfile:
            print(row[data],file=errorfile)

    except FileNotFoundError as _:
        print('Could not open/create',filename)
        print('Bailing :(')
        exit()


class OurDialect(csv.Dialect):
    delimiter      = ';'
    doublequote    = True
    lineterminator = '\r\n'
    quotechar      = '"'
    quoting        = csv.QUOTE_MINIMAL
    

def wrangle(input,cmd,pre,name,post,data,start_at,timeout=None,tick_callback=None,max_callback=None):
    if input is None:
        print("No filename given: use --input")
        return
    try:
        with open(input) as file:
            csv.field_size_limit(262144)
            reader = csv.reader(file,OurDialect())
            rows = [row for row in reader]
    except FileNotFoundError:
        print("File "+input+" not found")
        return
    

    N = len(rows)
    if max_callback: max_callback(N)
    with progressbar.ProgressBar(max_value=N,redirect_stdout=True) as bar:
        print("start",start_at)
        for i,row in enumerate(rows[start_at:]): # skip header
            if tick_callback: tick_callback(i)
            else: bar.update(i)
            process_row( row, cmd, pre, name, post, data, cmd_timeout=timeout )


if args.gui:
    start_gui(wrangle)
else:
    wrangle(
        args.input,
        args.cmd,
        args.pre,
        args.name,
        args.post,
        args.data,
        args.start_at,
        args.timeout
        )
