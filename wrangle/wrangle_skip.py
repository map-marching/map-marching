import csv
import subprocess
import argparse
import progressbar
import signal
from gui import start_gui

# handle commandline arguments
argparser = argparse.ArgumentParser(description='Runs an external command for every row in a CSV file and pipes each result to a file.')
argparser.add_argument('--cmd', help='Command to run.')
argparser.add_argument('--input', help='Filename of input CSV.')
argparser.add_argument('--name', '-n', metavar='COL', type=int, help='Get filename from which column?', default=0)
argparser.add_argument('--data', '-d', metavar='COL', type=int, help='Get data from which column?', default=2)
argparser.add_argument('--pre', help='Prefix for output filenames.', default='')
argparser.add_argument('--post', help='Postfix for output filenames.', default='')
argparser.add_argument('--start-at', '-s', metavar='ROW', type=int, help='First row to start working at; earlier rows are skipped.', default=1)
argparser.add_argument('--gui', action="store_true", help='Launch graphical user interface.')
args = argparser.parse_args()

def handler(signal_received, frame):
    raise KeyboardInterrupt("SIGTERM received")

def process_row(row,cmd,pre,name,post,data):
    command = [cmd]
    filename = pre + row[name] + post
    print('Writing',filename)
    try:
        with open(filename,'w') as outfile:
            print()
            print(filename)
            print(command)
            proc = subprocess.Popen( command, shell=True, stdin=subprocess.PIPE, stdout=outfile, universal_newlines=True )
            proc.communicate(input=row[data])
            proc.wait()
    except FileNotFoundError as _:
        print('Could not open/create',filename)
        print('Bailing :(')
        exit()


class OurDialect(csv.Dialect):
    delimiter      = ';'
    doublequote    = True
    lineterminator = '\r\n'
    quotechar      = '"'
    quoting        = csv.QUOTE_MINIMAL
    

def wrangle(input,cmd,pre,name,post,data,start_at,tick_callback=None,max_callback=None):
    if input is None:
        print("No filename given: use --input")
        return
    try:
        with open(input) as file:
            csv.field_size_limit(262144)
            reader = csv.reader(file,OurDialect())
            rows = [row for row in reader]
    except FileNotFoundError:
        print("File "+input+" not found")
        return
    

    N = len(rows)
    if max_callback: max_callback(N)
    with progressbar.ProgressBar(max_value=N,redirect_stdout=True) as bar:
        print("start",start_at)
        for i,row in enumerate(rows[start_at:]): # skip header
            try:
                if tick_callback: tick_callback(i)
                else: bar.update(i)
                process_row( row, cmd, pre, name, post, data )
            except:
                print("Continuing with next polygon")
                break


if args.gui:
    start_gui(wrangle)
else:
    signal.signal(signal.SIGTERM, handler)
    wrangle(
        args.input,
        args.cmd,
        args.pre,
        args.name,
        args.post,
        args.data,
        args.start_at
        )
