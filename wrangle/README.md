# Wrangle

Runs an external command for every row in a CSV file and pipes each result to a file.

## Setup

Run in this directory:

```
python -m pip install -r requirements.txt
```

## Example run

Put `constance_polys.csv` into the top directory and make a directory `processed`.
Now from that top directory, run `wrangle.py [OPTIONS] cmd input`.

By default it:
* Reads `input` which should be a CSV file in our dialect.
* Skips the first row (use `--start-row` to set start row)
* Takes the first column of each row as destination filename (pick other column with `--name`); prefixed by `--pre` and postfixed by `--post`.
* Takes the content of the second column (pick column with `--data`), runs `cmd` and pipes the data into it.
* Stdout from the process redirected into the file indicated before. File is created if it does not exist, or overwritten without warning; however, directory must already exist.

So for example:
```
python wrangle/wrangle.py --pre processed/ --post .tes --cmd "tessa/bin/tessa --gabriel" --input constance_polys.csv
```

Alternatively, use the GUI form:
```
python wrangle/wrangle.py --gui
```