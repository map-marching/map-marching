# (Small) Map Marching meeting 21.04.2021
## Attendees
* Axel Forsch
* Timon Behr

## Topics

* Evaluation techniques and data needed:
	* Compare our approach to the other approach:
		* Minimum number of points needed to store matched trajectory vs. Shape difference to original trajectory.
		* Length of unmatched segments.
		* Number of unmatched segments.
	* User study:
		* Easy option: How accurate is a single match?
		* Advanced option: Precision and recall with different parameter. (From expensive off-road and free-space to cheap off-road and free-space match costs).
	* Shape difference of free-space segments compared to simple street segments.
	* Data needed:
		* Relate matched points to edges and their respective types if any.
		* Relate original gpx points to matched points.
		* Relate matched points to edges and their respective types if any with different parameter.
		* Relate original gpx points to matched points on road segments and free-space segments.
