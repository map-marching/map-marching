# Map Marching meeting 26.11.2020
## Attendees
* Axel Forsch
* Timon Behr

## Topics
* Connect edges with close polygons 
	* Using another free space (additional step in the workflow)
	* Buffer polygons to find cuts
* Edges that cross free spaces
	* Add edges inside free spaces as additional polygons?
* Some things are still not recognized:
	* `bicycle = yes` e.g. in `highway: footpath`
* Indicator for coordinate system
	* epsg code as property

