# Map Marching meeting 31.03.2021
## Attendees
* Axel Forsch
* Timon Behr
* Thomas van Dijk

## Topics
* Trajectories matched on the currently final off-road network.
	* Problems:
		* Some trajectories still pass through obstacles.
		* Some road edges inside of triangulation are labeled as mash edges instead.
			* Solution: Updated tessa executable.
		* The triangulation is too coarse for a good matching.
			* Possible solutions:
				1. Lower off-road matching costs inside free-spaces instead of a triangulation.
				2. A more fine-grained triangulation.
				3. A grid instead of a triangulation.
				4. Implement 1. and 2. (1. with the tweak that off-road matching is forbidden) and compare both approaches.

# Small Catch Up Meeting
## Attendees
* Timon Behr
* Sabine Storandt

## Topics
* Review of earlier meeting.
	* Splitting free-space polygons into mosaic-graphs is important for indexing (and compression?).
* Other options for splitting polygons into mosaic-graphs.
	* Use middle points of each triangle in a triangulation.
	* 2D Straight Skeletons and Polygon Offsetting.
* Collecting more example trajectories.
* Sigspatial is more likely than giscience.
* Quality evaluation of the matching.
	* How to do it.
	* Similarity etc.
	* Implement
