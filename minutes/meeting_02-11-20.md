# Map Marching meeting 02.11.2020
## Attendees
* Sabine Storandt
* Jan-Henrik Haunert
* Thomas van Dijk
* Axel Forsch
* Timon Behr

## Topics
* The mesh of a polygon will be written to a single file named by the respective `osm_id`.
* Discussion to use the dual-graph of the triangulation instead of the triangulation itself.
* Fixed inner edges of a free polygon should be cheaper than other inner edges, and inner edges shoule be cheaper than marginal edges.

## Tasks
* Link to folder with extracts (in the git readme) does not work.
* Points of the outer way of a free polygon should be considered for the triangulation.
* Add ways that go through free polygons as (inner) ways (that do not close). Make sure that they are cut with the margin of the respective polygon.

## Next meeting
* The date for the next meeting will be set by Thomas.

