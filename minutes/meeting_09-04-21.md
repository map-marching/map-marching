# Map Marching meeting 09.04.2021
## Attendees
* Sabine Storandt
* Jan-Henrik Haunert
* Axel Forsch
* Timon Behr
* Thomas van Dijk

## Topics
* GIScience or no GIScience?
	* Deadline to short.
	* Project could be too practical for Sigspatial.
		* Which could be fixed by introducing a sophisticated evaluation approach.
	* Solution: We first do a GIScience short paper, then a journal article.

* Why do we want to match trajectories to a simple graph instead of treating free-space segments as off-road segments or free-spaces as hyper-edges?
	* Many GIS approaches require a simple graph matching.
		* Indexing
		* Compression
		* Finding shared paths of different trajectories.

* Evaluation options
	* We have ground truth.
		* (Small expert study)
	* Compare matched with original.
	* Compare result with approach from Haunert and Budig.
	* Shape and distance
	* How often is a trajectory cut (by a off-road segment).
	* Dynamic time window

* How to improve the triangulation:
	* Mashing
	* Skeletons

* Next meeting:
	* 23.04, 14:30 
	* Goals:
		* Outline for the paper.
		* Pipeline with new tessa and mashing.
		* Actual evaluation approach.

