# Map Marching meeting 19.10.2020
## Attendees
* Sabine Storandt
* Jan-Henrik Haunert
* Thomas van Dijk
* Axel Forsch
* Timon Behr
## Topics
* Representation of the triangulation
		* As a graph in the same form of the road network (prefered).
				* Problem: referencing between polygons and edges.
		* As wkt tin.
				* Problem: inefficient
* mash datatyp
* Streets that pass through free spaces should be used as mendetory edges in the trianglulation. 
* One reason for the triangulation of free spaces is the circumvention of obstacles.
* Points of intrest adjacent to free spaces could be used as nodes in the triangulation.
* First step: onroad map-matching with mesh and road network.
## Tasks
* Aggregation of free spaces.
* Connect mesh and road network to a single network.
* Cloud folder for off-road trajectories.
* Text to shapefile transformation.
* Obstacles as shapefiles (later).
## Next meeting
* 02.11.2020

