# (Small) Map Marching meeting 12.04.2021
## Attendees
* Axel Forsch
* Timon Behr

## Topics
* Extended long paper deadline.
	* We could still make it.

* Paper outline

* Next (small) meeting:
	* 19.04, 10:00 
	* Tasks:
		* Timon:
			* Collection of user data (actual taken paths).
			* Implementation of evaluation approaches.
		* Axel:
			* Introduction draft.
			* Algorithm to find meaningful segments in given trajectories.

# (Small) Map Marching discussion 12.04.2021
## Attendees
* Sabine Storandt
* Timon Behr

## Topics
* Extended long paper deadline.

* Paper outline
	* Methodology:
		* Could be split in two
		* Main steps of the pipeline should go into this chapter, details can go into the experiments chapter.

* Evaluation

* Expert study:
	* Only let the user define the first and last point where they left the designated paths
	* Sensitivity analysis

* Evaluation measures:
	* Dynamic time wrapping
	* Frechet distance should be done with a library
	* Count holes
	* Measure the length of holes


