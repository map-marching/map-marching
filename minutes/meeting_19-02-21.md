# Map Marching meeting 19.02.2021
## Attendees
* Axel Forsch
* Timon Behr
* Sabine Storandt
* Jan-Henrik Haunert

## Topics
* Presentation and discussion of the gui:
	* Interactive parameter adjustment:
		* offroad cost: factor expensiveness to walk off road.
		* candidate cost: factor expensiveness to distance from candidate point.
		* tessellation cost: factor expensiveness to walk on tessellation.
		* obstacle boundary cost: factor expensiveness to walk along the boundary of an obstacle.
	* Ideas:
		* Real time adjustment of matched trajectories.
		* Shadow of former matched trajectory.
* GI science:
	* Discuss with Thomas
	* Steps to paper:
		* Tessellation
		* Incorporate tessellation
		* Parameter tuning
		* Evaluate
		* Theory and proofs
		* Write it all down

* Tasks:
	* Finish and incorporate tessellation.
	* Extract information for complete Constance + Meersburg + ferry connection.
	* Allow roads through obstacles.
	* Git repo for GI science document.

* Next small meeting as soon as tessellation is done.



