# Map Marching meeting 11.12.2020
## Attendees
* Axel Forsch
* Timon Behr
* Sabine Storandt
* Thomas van Dijk

## Topics
* Trajectories need to avoid obstacles.
	* First point before obstacle / first point after obstacle
* Graph edges that cut, or go through free spaces need to be included for triangulation.
	* In WKT: as a line string together with the polygon inside a geometry collection.
* Buffer free spaces polygons to find possible transitions from road segments to free space. (Before cutting with obstacles)
* Non-convex free spaces and/or free spaces with holes are not correctly triangulated.
* Collect off road trajectories in Constance.
* Which osm tags can still be included? E.g. water as obstacle.

