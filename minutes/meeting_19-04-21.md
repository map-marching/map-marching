# (Small) Map Marching meeting 19.04.2021
## Attendees
* Axel Forsch
* Timon Behr

## Topics
* Mesh
	* Can we tweak at which width a polygon is split into two rows of triangles?
	* Free-spaces inside free-spaces are still a problem.
		* Cut free-spaces.
		* Or merge them.

* Evaluation:
	* Shape (distance) measures in comparison to oder matches (where off-road segments that are not matched at all, are used in the original form).

