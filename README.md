We consider the problem of matching trajectories to a road map, giving particular consideration to trajectories that do not exclusively follow the underlying network.
Such trajectories arise, for example, when a person walks through the inner part of a city, crossing market squares or parking lots.
We call such trajectories semi-restricted.
Sensible map matching of semi-restricted trajectories requires the ability to differentiate between restricted and unrestricted movement.
We develop in this paper an approach that efficiently and reliably computes concise representations of such trajectories that maintain their semantic characteristics.
Our approach utilizes OpenStreetMap data to not only extract the network but also areas that allow for free movement (as e.g. parks) as well as obstacles (as
e.g. buildings).
We discuss in detail how to incorporate this information in the map matching process, and demonstrate the applicability of our method in an experimental evaluation on real pedestrian and bicycle trajectories.

![strandbad-horn](./img/strandbad-horn.png "Matching for a semi-restriced trajectory")

*Matching result for a trajectory through a park.
The recorded trajectory (blue crosses) leaves the waymarked paths (black lines), but a feasible matching (yellow dashed) can be found thanks to the extended network model (gray lines).*

<hr/>

## Software and Data

* [![github logo][ghg] Github](https://github.com/tcvdijk/tessa) and [![software heritage logo][sh] Software Heritage (archived)](https://archive.softwareheritage.org/swh:1:dir:265a254805a64ab73036cdebd99b59590052a471;origin=https://github.com/tcvdijk/tessa;visit=swh:1:snp:97d67a9d7b3dc4e59f0c4c054930e08bced994dc;anchor=swh:1:rev:4ba5ad0a8d72f0530e12f463008fccb376c12d89): tool used to generate the tesselation of the free spaces

<hr/>

[sh]: https://gitlab.vgiscience.de/map-marching/map-marching/-/raw/master/assets/software-heritage.svg
[ghg]: https://gitlab.vgiscience.de/vgi-routing/vgi-routing/-/raw/master/assets/GitHub-gray.svg
