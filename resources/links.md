# Useful and important links

## Test and example files
Resources like example files can be stored and found [here](https://cloud.uni-konstanz.de/index.php/s/LT7dtDkw7b9Grc9)

## Useful links
* [OsmToRoadGraph](https://github.com/AndGem/OsmToRoadGraph) python script to convert .osm files to ascii files representing the road network
* [Geofabrik](https://download.geofabrik.de/) daily extracts of osm data
