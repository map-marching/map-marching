from miniipe import Document, Scale, circle, segment, symbol_name
import csv
import argparse
from math import inf
from sys import stderr

argparser = argparse.ArgumentParser(description='Converts our little graph format to IPE.')
argparser.add_argument('input', help='Graph file.')

args = argparser.parse_args()

class OurDialect(csv.Dialect):
    delimiter      = ';'
    doublequote    = True
    lineterminator = '\r\n'
    quotechar      = '"'
    quoting        = csv.QUOTE_MINIMAL
with open(args.input) as file:
    reader = csv.reader(file,OurDialect())
    rows = [row for row in reader]

n = int(rows[0][0])
m = int(rows[1][0])

class BoundsAccumulator(object):
    min = [inf,inf]
    max = [-inf,-inf]
    def accumulate(self,p):
        if p[0]<self.min[0]: self.min[0] = p[0]
        if p[1]<self.min[1]: self.min[1] = p[1]
        if p[0]>self.max[0]: self.max[0] = p[0]
        if p[1]>self.max[1]: self.max[1] = p[1]
    def page(self, slack=0, move_to_origin=False):
        h = self.max[0] - self.min[0]
        w = self.max[1] - self.min[1]
        if move_to_origin:
            return (0,0), (h,w)
        else:
            ox = -self.min[0]+slack*w
            oy = -self.min[1]+slack*h
            px = self.max[0]-self.min[0]+2*slack*w
            py = self.max[1]-self.min[1]+2*slack*h
            return (ox,oy), (px,py)


doc = Document()
doc.add_color_rgb( 'black', 0, 0, 0 )
doc.add_color_rgb( 'boundary', 0, 0.7, 0 )
doc.add_color_rgb( 'hole', 0.8, 0, 0 )
doc.add_color_rgb( 'road', 0.3, 0.3, 1 )
doc.add_color_rgb( 'mesh', 0.7, 0.7, 0.7 )

# Accumulate bounds
bounds = BoundsAccumulator()
for node in rows[2:2+n]:
    p = ( float(node[1]), float(node[2]) )
    bounds.accumulate(p)

# draw nodes
o = symbol_name('mark/o', size=True)
o_symbol = doc.add_symbol(o)
doc.path( circle((0,0),1), parent=o_symbol)
nodes = [_ for _ in range(n)]
doc.add_layer('nodes')
for node in rows[2:2+n]:
    id = int(node[0])
    p = ( float(node[1])-bounds.min[0], float(node[2])-bounds.min[1] )
    nodes[id] = p
    doc.use(o,pos=p,layer='nodes')

# draw edges
doc.add_layer('boundary')
doc.add_layer('hole')
doc.add_layer('road')
doc.add_layer('mesh')
for edge in rows[2+n:]:
    i = int(edge[0])
    j = int(edge[1])
    layer = edge[5]
    color = edge[5]
    doc.path( segment(nodes[i],nodes[j]), layer=layer, stroke=color )


origin, page = bounds.page(slack=0.1, move_to_origin=True)
doc.add_layout( origin=origin, page=page )

print(doc.tostring())