# The "pipeline"

In this document, instructions for all data preperation steps of our
map marching pipeline as well as the final matching are given.

1. Download OSM-extracts:
	- Tools: internet browser
	- [Geofabrik Download](https://download.geofabrik.de/)
	- Output: `.osm.bz2` file
   - Coordinates: WGS84, EPSG:4326
	
2. Crop data to experimental region:
   - Tools: [Osmosis](https://wiki.openstreetmap.org/wiki/Osmosis)
   - Input: `.osm.bz2` file (see step 1), bounding box in lonLat of experimental region
   - Examplary call: `bzcat freiburg-regbez-latest.osm.bz2 | ~/Documents/osmosis/bin/osmosis --read-xml enableDateParsing=no file=- --bounding-box top=47.8 left=9 bottom=47.6 right=9.2 --write-xml file=- | bzip2 > extracted.osm.bz2`
   - Output: `extracted.osm.bz2`
   - Coordinates: WGS84, EPSG:4326
   
3. Generate polygons (freespaces/obstacles) from OSM data
   - Tools: [OsmToPolgons](https://gitlab.vgiscience.de/map-marching/osmtopolygons)
   - Input: `.osm.bz2` (see step 1 or 2)
   - Parameters: `-f` .. input file, `-o` .. output file
   - Examplary call: `osmtopoly polygons -f 001_constance.osm.bz2 -o 002_constance`
   - Output: `csv`-file with fields: `osm_id`, `wkt_polygon`, `free_space_for`, `obstacle_for`, the last two columns list for which types of transporation these polygons are free-spaces or obstacles
   - Coordinates: WGS84, EPSG:4326

4. Generate road graph from OSM data 
   - Tools: [OsmToPolgons](https://gitlab.vgiscience.de/map-marching/osmtopolygons)
   - Input: `.osm.bz2` (see step 1 or 2)
   - Parameters: 
     - `-f` .. input file
     - `-o` .. output file
     - `-m` .. mode: one of `[pedestrian, bicycle, motor_vehicle]`
   - Examplary call: `osmtopoly graph -f 001_constance.osm.bz2 -o 002_constance_bicycle -m bicycle`
   - Output: `.ssg` file, information about `.ssg` in next chapter
   - Coordinates: WGS84, EPSG:4326

5. Transform graph to UTM
   - Tools: [polygonConverter](https://gitlab.vgiscience.de/map-marching/osmtopolygons/-/tree/master/polygonConverter)
   - Input: `.ssg` file in EPSG:4326 (see step 4)
   - Parameters:
     - `-f` .. input file
     - `-o` .. output file
     - `-epsg` .. epsg code of target coordinate system
   - Examplary call: `java -jar GraphConverter.jar -f 002_constance_bicycle.ssg -epsg 25832 -o 003_constance_bicycle_UTM.ssg`
   - Output: `.ssg` file in target coordinate system
   - Coordinates: UTM32, EPSG:25832

6. Cut freespaces such that they do not overlap obstacles
   - Tools: [polygonConverter](https://gitlab.vgiscience.de/map-marching/osmtopolygons/-/tree/master/polygonConverter)
   - Input: `.csv` with polygons (see step 3)
   - Parameters:
     - `-f` .. input file
     - `-o` .. output file
     - `-m` .. mode: one of `[0 = pedestrian, 1 = bicycle, 2 = motor_vehicle]`
   - Examplary call: `java -jar PolygonCrop.jar -f 002_constance.csv -o 004_constance_bicycle.csv -m 1`
   - Output: `.csv` file without overlapping freespaces
   - Coordinates: UTM32, EPSG:25832
   - Note: this function also converts to UTM

7. Filter freespaces
	- Tools: Can be done with a simple script (see [this](https://gitlab.vgiscience.de/map-marching/osmtopolygons/-/tree/master/helper))
	- Input: `.csv` file without overlapping freespaces (see step 6)
	- Examplary call: `python3 csv_pruner.py 004_constance_bicycle.csv 005_constance_bicycle_free.csv 005_constance_bicycle_obstacle`
	- Output: `.csv` file with the respective freespaces
	- Coordinates: UTM32, EPSG:25832

8. Cut out the edges of the road network that pass through freespace
  - Tools: [free_space_streets](https://gitlab.vgiscience.de/map-marching/osmtopolygons/-/tree/master/free_space_streets)
	- Input: `.csv` with freespaces and `.ssg` with the road graph of the correct mode. Both in UTM (see step 7 and 5)
	- Parameters:
		- `-g` .. input graph
		- `-p` .. input polygons 
		- `-c` .. output csv file
		- `-s` .. output ssg file
	- Examplary call: `./target/release/free_space_streets -g 003_constance_bicycle_UTM.ssg -p 005_constance_bicycle_free.csv -c 006_constance_bicycle_free.csv -s 007_constance_UTM.ssg`
	- Output: `.csv` file containing wkt GeometryCollection with polygons and multilinestrings and `.ssg` file containing the left over graph.
	- Coordinates: UTM32, EPSG:25832

9. Calculate the tessalation of all freespaces:
   - Tools: [tessa](https://gitlab.vgiscience.de/map-marching/map-marching/-/tree/master/tessa) and [wrangle](https://gitlab.vgiscience.de/map-marching/map-marching/-/tree/master/wrangle)
   - Input: `.csv` non-overlapping freespaces (see step 6)
   - Parameters:
     - `-pre` .. prefix for output
     - `-post` .. postfix for output
     - Tessallation type: e.g. `--gabriel`
   - Examplary call: `python3 path/to/wrangle.py --pre processed/ --post .tes --cmd "path/to/tessa/bin/tessa --cdt --free-for bicycle" --input path/to/polygons.csv`
   - Output: one `.ssg` file for each polygon in the input, named by its `osm_id`

10. Merge road graph with tessellation graphs
	- Tools: [OsmToPolgons](https://gitlab.vgiscience.de/map-marching/osmtopolygons)
	- Input `.ssg` file with road graph and `.ssg` files with tessellation graphs
	- Parameters:
		- Input files, starting with the road network
		- `-o` .. output file
	- Examplary call: `osmtopoly netmerge 007_constance_UTM.ssg path/to/tessa/files/\* -o 009_constance_offroad_bicycle_free`
	- Output: offroad graph
	- Coordinates: UTM32, EPSG:25832

